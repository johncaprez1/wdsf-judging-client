﻿using System;
using System.Collections.Generic;
using System.Windows;

using GrandSlamTransmitter.Model;

using Wdsf.Api.Client;
using Wdsf.Api.Client.Models;

using Competition = GrandSlamTransmitter.Model.Competition;

namespace GrandSlamTransmitter.Transmitter
{
    public abstract class ResultTransmitAbstract
    {
        #region Static Fields

        protected static Client apiClient;

        #endregion

        #region Fields

        protected List<OfficialDetail> _officals;

        protected string _round;

        protected Dictionary<string, string> translateDanceName;

        #endregion

        #region Constructors and Destructors

        public ResultTransmitAbstract(string user, string password)
        {
#if DEBUG
            apiClient = new Client("ogroehn", "Nuwanda01", WdsfEndpoint.Sandbox);
#else
            apiClient = new Client(user, password, WdsfEndpoint.Services);
#endif
            this.translateDanceName = new Dictionary<string, string>
                                          {
                                              { "SW", "WALTZ" },
                                              { "W", "WALTZ" },
                                              { "TG", "TANGO" },
                                              { "VW", "VIENNESE WALTZ" },
                                              { "SF", "SLOW FOXTROT" },
                                              { "QS", "QUICKSTEP" },
                                              { "SB", "SAMBA" },
                                              { "CC", "CHA CHA CHA" },
                                              { "RB", "RUMBA" },
                                              { "PD", "PASO DOBLE" },
                                              { "JV", "JIVE" },
                                              { "SA", "SALSA" },
                                              { "SS", "SHOWDANCE" },
                                              { "SL", "SHOWDANCE" },
                                              { "FL", "FORMATION LATIN" },
                                              { "FS", "FORMATION STANDARD" }
                                          };
        }

        #endregion

        #region Public Methods and Operators

        public void SendResult(Competition competition, string round, string minChairman, bool clearBeforeSending)
        {
            Logger.Instance.Log(Logger.Level.Info, "Result Transmission started");
            this._round = round;
            // Wir senden zu diesem Turnier nun die Daten an den WDSF Server
            int wdsfId = 0;
            if (!Int32.TryParse(competition.WDSF_Id, out wdsfId))
            {
                MessageBox.Show(
                    "Competition " + competition.Title
                    + " can not be send to WDSF because Competition Number is not numeric");
                return;
            }

            CompetitionDetail wdsfCompetition = apiClient.GetCompetition(wdsfId);

            Logger.Instance.Log(
                Logger.Level.Info,
                String.Format(
                    "Registering for competition {0} {1} {2} in {3}-{4} on {5}",
                    wdsfCompetition.CompetitionType,
                    wdsfCompetition.AgeClass,
                    wdsfCompetition.Discipline,
                    wdsfCompetition.Location,
                    wdsfCompetition.Country,
                    wdsfCompetition.Date));

            // Let's load the offials from the WDSF database
            IList<Official> officials = apiClient.GetOfficials(wdsfCompetition.Id);
            // Clear all officials:
            if (clearBeforeSending)
            {
                foreach (var official in officials)
                {
                    try
                    {
                        apiClient.DeleteOfficial(official.Id);
                    }
                    catch (Exception ex)
                    {
                        Logger.Instance.Log(ex);
                    }
                    
                }
                officials.Clear();
            }

            if (officials.Count < 10)
            {
                this.RegisterOfficials(competition, wdsfCompetition);
                officials = apiClient.GetOfficials(wdsfCompetition.Id);
            }

            // Register Extra Judge
            //RegisterJudge(wdsfCompetition, new OfficalWithRole()
            //                                   {
            //                                       EventId = 0,
            //                                       Offical = new Offical()
            //                                                     {
            //                                                         Sign = "G",
            //                                                         Club = "Denmark",
            //                                                         FirstName = "Frank",
            //                                                         LastName = "Radich",
            //                                                         MIN = 10000347,
            //                                                         WDSFSign = "X"
            //                                                     }
            //                                   });
            if (!string.IsNullOrEmpty(minChairman))
            {
                this.RegisterChairman(int.Parse(minChairman), wdsfCompetition);
            }

            this._officals = new List<OfficialDetail>();
            foreach (Official official in officials)
            {
                this._officals.Add(apiClient.GetOfficial(official.Id));
            }

            this.UpdateResults(competition, wdsfCompetition, clearBeforeSending);

            Logger.Instance.Log(Logger.Level.Info, "Result Transmission finised");
        }

        public void SetStateClosed(Competition competition)
        {
            int wdsfId = 0;
            if (!Int32.TryParse(competition.WDSF_Id, out wdsfId))
            {
                Logger.Instance.Log(
                    Logger.Level.Error,
                    competition.Title + " can not be send to WDSF because Competition Number is not numeric");
                return;
            }
            CompetitionDetail wdfCompetition = apiClient.GetCompetition(wdsfId);

            wdfCompetition.Status = "Closed";
            bool res = apiClient.UpdateCompetition(wdfCompetition);
            if (!res)
            {
                Logger.Instance.Log(
                    Logger.Level.Warning,
                    "Could not update State of Competition " + competition.Title + ": " + apiClient.LastApiMessage);
            }
        }

        public void SetStateInProgress(Competition competition)
        {
            int wdsfId = 0;
            if (!Int32.TryParse(competition.WDSF_Id, out wdsfId))
            {
                Logger.Instance.Log(
                    Logger.Level.Warning,
                    competition.Title + " can not be send to WDSF because Competition Number is not numeric");
            }
            CompetitionDetail wdsfCompetition = apiClient.GetCompetition(wdsfId);

            wdsfCompetition.Status = "InProgress";

            bool res = apiClient.UpdateCompetition(wdsfCompetition);
            if (!res)
            {
                Logger.Instance.Log(
                    Logger.Level.Warning,
                    "Could not update State of Competition " + competition.Title + ": " + apiClient.LastApiMessage);
            }
        }

        public void SetStateProcessing(Competition competition)
        {
            int wdsfId = 0;
            if (!Int32.TryParse(competition.WDSF_Id, out wdsfId))
            {
                Logger.Instance.Log(
                    Logger.Level.Warning,
                    competition.Title + " can not be send to WDSF because Competition Number is not numeric");
            }
            CompetitionDetail wdsfCompetition = apiClient.GetCompetition(wdsfId);

            wdsfCompetition.Status = "Processing";
            bool res = apiClient.UpdateCompetition(wdsfCompetition);
            if (!res)
            {
                Logger.Instance.Log(
                    Logger.Level.Warning,
                    "Could not update State of Competition " + competition.Title + ": " + apiClient.LastApiMessage);
            }
        }

        #endregion

        #region Methods

        protected abstract void UpdateResults(Competition competition, CompetitionDetail wdsfCompetition, bool clearBoforSending);

        private void RegisterChairman(int minChairman, CompetitionDetail wdsfCompetition)
        {
            var official = new OfficialDetail
                               {
                                   AdjudicatorChar = "CM",
                                   CompetitionId = wdsfCompetition.Id,
                                   Min = minChairman,
                                   Task = "Chairman",
                               };
            try
            {
                apiClient.SaveOfficial(official);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex);
                if (ex.InnerException != null)
                {
                    Logger.Instance.Log(ex.InnerException);
                }
            }
        }

        private void RegisterOfficials(Competition competition, CompetitionDetail wdsfCompetition)
        {
            foreach (OfficalWithRole judge in competition.Judges)
            {
                RegisterJudge(wdsfCompetition, judge);
            }
        }

        private static void RegisterJudge(CompetitionDetail wdsfCompetition, OfficalWithRole judge)
        {
            var official = new OfficialDetail
                               {
                                   AdjudicatorChar = judge.Offical.WDSFSign,
                                   CompetitionId = wdsfCompetition.Id,
                                   Min = judge.Offical.MIN,
                                   Task = "Adjudicator",
                                   Nationality = judge.Offical.Club,
                                   Person = judge.Offical.Name
                               };
            try
            {
                apiClient.SaveOfficial(official);
            }
            catch (Exception ex)
            {
                Logger.Instance.Log(ex);
                if (ex.InnerException != null)
                {
                    Logger.Instance.Log(ex.InnerException);
                }
            }
        }

        #endregion
    }
}