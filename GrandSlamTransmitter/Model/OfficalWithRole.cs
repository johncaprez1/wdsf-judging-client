﻿namespace GrandSlamTransmitter.Model
{
    public class OfficalWithRole
    {
        public Offical Offical { get; set; }
        public string Role { get; set; }
        public int Id { get; set; }
        public int EventId { get; set; }
    }
}