﻿using System;

namespace GrandSlamTransmitter.Model
{
    public class Event
    {
        public int WdsfId { get; set; }
        public string Place { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateUntil { get; set; }
    }
}