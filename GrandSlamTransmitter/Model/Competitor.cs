﻿using System.Collections.Generic;

namespace GrandSlamTransmitter.Model
{
    public class Competitor
    {
        public int Star { get; set; }


        public Couple Couple { get; set; }

        public Competition Competition { get; set; }

        public object WDSFPartisipant { get; set; }

        public int Number { get; set; }

        public int PlaceFrom { get; set; }

        public int PlaceTo { get; set; }

        public double TotalMarking { get; set; }

        public Dictionary<string, List<Mark>> Results { get; set; }

        public string LastRound { get; set; }
    }
}