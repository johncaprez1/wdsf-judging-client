﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace GrandSlamTransmitter
{
    class Logger
    {
        public enum Level
        {
            Error = 0,
            Warning = 1,
            Info = 2
        }

        private string _file;
        private object _lockObject;

        public string File
        {
            get { return _file; }
        }

        private static Logger _instance;
        public static Logger Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Logger();
                return _instance;
            }
        }

        private Logger()
        {
            _file = System.IO.Path.GetTempFileName();
            _lockObject = new object();
        }

        public void Log(Exception ex)
        {
            if (ex.InnerException != null)
            {
                Log(ex.InnerException);
                return;
            }

            Log(Level.Error, ex.Message);
        }

        public void Log(Level level, string message)
        {
            string sLevel = "";
            switch (level)
            {
                case Level.Info:
                    sLevel = "Info ";
                    break;
                case Level.Warning:
                    sLevel = "Warn ";
                    break;
                case Level.Error:
                    sLevel = "Error";
                    break;
            }
            lock (_lockObject)
            {
                var stream = new StreamWriter(_file, true);
                stream.WriteLine("[{0} {1}] [{2}] - {3}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), sLevel, message);
                stream.Close();
            }
        }
    }
}
