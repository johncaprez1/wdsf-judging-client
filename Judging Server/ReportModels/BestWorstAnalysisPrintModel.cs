﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;

namespace Judging_Server.ReportModels
{
    public class JudgeBestWorsPrintModel
    {
        public string JudgeSign { get; set; }
        public double DistanceMedia { get; set; }
        public bool IsToFar { get; set; }
    }

    public class JudgeAreaViewModel
    {
        public JudgeBestWorsPrintModel Worst { get; set; }
        public JudgeBestWorsPrintModel Media { get; set; }
        public JudgeBestWorsPrintModel Best { get; set; }
    }

    public class BestWorstAnalysisPrintModel
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public string Dance { get; set; }

        public JudgeAreaViewModel BestWorstAreaA { get; set; }
        public JudgeAreaViewModel BestWorstAreaB { get; set; }
        public JudgeAreaViewModel BestWorstAreaC { get; set; }
        public JudgeAreaViewModel BestWorstAreaD { get; set; }

        public SolidColorBrush Background
        {
            get
            {
                var codeRed = BestWorstAreaA.Best.DistanceMedia > 1.49 || BestWorstAreaA.Worst.DistanceMedia > 1.49 ||
                              BestWorstAreaB.Best.DistanceMedia > 1.49 || BestWorstAreaB.Worst.DistanceMedia > 1.49 ||
                              BestWorstAreaC.Best.DistanceMedia > 1.49 || BestWorstAreaC.Worst.DistanceMedia > 1.49 ||
                              BestWorstAreaD.Best.DistanceMedia > 1.49 || BestWorstAreaD.Worst.DistanceMedia > 1.49;

                var codeYellow = BestWorstAreaA.Best.DistanceMedia > .99 || BestWorstAreaA.Worst.DistanceMedia > .99 ||
                              BestWorstAreaB.Best.DistanceMedia > .99 || BestWorstAreaB.Worst.DistanceMedia > .99 ||
                              BestWorstAreaC.Best.DistanceMedia > .99 || BestWorstAreaC.Worst.DistanceMedia > .99 ||
                              BestWorstAreaD.Best.DistanceMedia > .99 || BestWorstAreaD.Worst.DistanceMedia > .99;

                if(codeRed)
                    return new SolidColorBrush(Colors.Red);
                if(codeYellow)
                    return new SolidColorBrush(Colors.Yellow);

                return new SolidColorBrush(Colors.White);
            }
        }
    }
}
