﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.ReportModels
{
    public class ResultPrintModel
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public double ComponentA { get; set; }
        public double ComponentB { get; set; }
        public double ComponentC { get; set; }
        public double ComponentD { get; set; }
        public double Points { get; set; }
        public int Place { get; set; }
    }
}
