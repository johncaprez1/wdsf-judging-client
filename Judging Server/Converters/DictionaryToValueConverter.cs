using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Judging_Server.Model
{
    public class DictionaryToValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var sIndex = parameter as string;

            if (sIndex == null) return null;

            var index = 0;
            if (!Int32.TryParse(sIndex, out index)) return null;



            var dict = (Dictionary<string, TotalPerCouple>) value;
            if (dict == null) return null;
            if (index >= dict.Keys.Count) return null;

            var key = dict.Keys.ToList()[index];

            return dict[key].TotalAndArea;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}