﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

using Nancy;

namespace Judging_Server.Nancy
{
    public class FileHandlerModule : NancyModule
    {
        public FileHandlerModule()
            : base("/FileHandler")
        {
            this.Get["/"] = _ =>
                {
                    // Log the IP Address to local AvailableDevices.txt
                    var source = this.Request.UserHostAddress;

                    try
                    {
                        var writer = File.AppendText(ServerSettings.Default.DataPath + "\\AvailableDevices.txt");
                        writer.WriteLine("A;http://{0}:9210;0", source);
                        writer.Close();
                    }
                    catch (Exception exception)
                    {
                        Debug.WriteLine(exception.Message);
                    }

                    return "Htpp Server is running";
                };

            this.Post["/Write/{filename}"] = request =>
                {
                    var filename = request.filename.ToString();
                    var eventId = this.Request.Query["EventId"];

                    try
                    {
                        var body = this.Request.Body;
                        int bl = (int)body.Length; //this is a dynamic variable.
                        byte[] data = new byte[bl];
                        body.Read(data, 0, bl);

                        var folder = string.Format("{0}\\{1}", ServerSettings.Default.DataPath, eventId);

                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }

                        File.WriteAllBytes(folder + "\\" + filename, data);

                        return "OK";
                    }
                    catch (Exception exception)
                    {
                       Debug.WriteLine(exception.Message);
                        return "ERROR: " + exception.Message;
                    }
                };
        }
    }
}
