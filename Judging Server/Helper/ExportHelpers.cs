﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Judging_Server.Model;
using System.IO;
using System.Net;
using System.Threading;

using Judging_Server.Nancy;

namespace Judging_Server.Helper
{
    /// <summary>
    /// Export helpers
    /// </summary>
    public class ExportHelpers
    {

        public void WriteHeatsV1(StreamWriter writer, DataContextClass context)
        {
            int i = 0;
            foreach (var dance in context.Heats.Keys)
            {
                var danceEntity = context.Dances.Single(d => d.ShortName == dance);
                writer.Write((i + 1).ToString() + ";" + danceEntity.ShortName + ";" + danceEntity.LongName);
                // Wir brauchen die Paare nun in diesem Tanz, bzw. deren Reihenfolge
                var heats = context.Heats[dance];
                // We only have solo dances so we have x heats with each one couple
                foreach (var heat in heats)
                {
                    foreach (var couple in heat.Couples)
                    {
                        writer.Write(";" + couple.Id);
                    }
                }
                writer.WriteLine();
                i++;
            }
        }

        /// <summary>
        /// Writes the judge drawing.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="path">The path.</param>
        /// <param name="device">The device.</param>
        public void WriteJudgeData(DataContextClass context, string path, string filename, DeviceStates device)
        {
            if (context.JudgeDrawing == null && context.CalculationVersion != CalculationVersionEnum.Version1)
            {
                var drawer = new Helper.CreateJudgesDrawing();
                context.JudgeDrawing = drawer.Create(context.JudgingAreas, context.Judges, context.Heats, 3, 4);
            }

            StreamWriter writer;
            MemoryStream memoryStream = new MemoryStream();

            if (path.ToLower().StartsWith("http://"))
            {
                writer = new StreamWriter(memoryStream);
            }
            else
            {
                writer = new StreamWriter(path + "//" + filename);
            }

            writer.WriteLine(context.CalculationVersion == CalculationVersionEnum.Version1 ? "V1" : "V2");
            // Write the subfolder name where the client should write it's data to ...
            writer.WriteLine("{0}_{1}", context.Event.ScrutinusId, context.Event.RoundNr);

            if (context.CalculationVersion == CalculationVersionEnum.Version2014)
            {
                // Max, Min Values
                writer.WriteLine("{0};{1};{2}", context.MinimumPoints, context.MaximumPoints, context.IntervallPoints);
            }
            // Write the Judging Components:
            writer.Write("{0}", context.JudgingAreas[0].Component);
            for (var j = 1; j < context.JudgingAreas.Count; j++)
            {
                writer.Write(";{0}", context.JudgingAreas[j].Component);
            }
            writer.WriteLine("");
            for (var j = 0; j < context.JudgingAreas.Count; j++)
            {
                writer.WriteLine("{0};{1}", context.JudgingAreas[j].Component, context.JudgingAreas[j].Description);
            }
            // we write the data to the file:
            Model.Judge judge = context.Judges.SingleOrDefault(j => j.Sign == device.Judge);
            // if we do not know the judge, this device might be the chairman's device
            if (judge != null)
            {
                writer.WriteLine("{0};{1};{2}", judge.Sign, judge.Name, judge.Country);
                writer.WriteLine(context.Event.Title);
            }
            else
            {
                MessageBox.Show("Found judge " + device.Judge + " in devices but not in data.txt. Intended?");
                writer.WriteLine("{0};{1};{2}", "CH", "Chairman", "");
                writer.WriteLine(context.Event.Title);
            }
            // write dances
            if(context.CalculationVersion == CalculationVersionEnum.Version1)
            {
                WriteHeatsV1(writer, context);
                writer.Close();
                return;
            }

            int i = 0;
            foreach (var dance in context.Heats.Keys)
            {
                var danceEntity = context.Dances.Single(d => d.ShortName == dance);
                writer.Write((i + 1) + ";" + danceEntity.ShortName + ";" + danceEntity.LongName);
                // Wir brauchen die Paare nun in diesem Tanz, bzw. deren Reihenfolge
                var heats = context.Heats[dance];
                var drawingList = context.JudgeDrawing[dance];
                // Anzahl der Heats geben wir noch aus
                writer.Write(";" + heats.Count);
                foreach (var heat in heats)
                {
                    var drawing = drawingList.Single(j => j.Judges.Any(ju => ju.Sign == judge.Sign && j.Heat.Id == heat.Id));
                    writer.Write(";" + heat.Couples.Count);
                    writer.Write(";" + drawing.Area);
                    
                    foreach (var couple in heat.Couples)
                    {
                        writer.Write(";" + couple.Id);
                    }
                }
                writer.WriteLine();
                i++;
            }
            writer.Close();

            if (path.ToLower().StartsWith("http://"))
            {
                var data = Encoding.Default.GetString(memoryStream.ToArray());
                WriteToServer(path, filename, data);
            }
        }

        public static void WriteToServer(string path, string filename, string data)
        {
            var webClient = new WebClientWithTimeOut();
            var errorCount = 0;
            while (true)
            {
                try
                {
                    webClient.UploadString(new Uri(path + "/FileHandler/Write/" + filename), "POST", data);
                    return;
                }
                catch (Exception)
                {
                    errorCount++;
                    if (errorCount > 0)
                    {
                        return;
                    }
                    Thread.Sleep(1000);
                }
            }
            
        }

        /// <summary>
        /// Writes the heat data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="path">The path.</param>
        public void WriteHeatData(DataContextClass context, string path)
        {
            var sw = new StreamWriter(path);
        
            foreach (var dance in context.Heats.Keys)
            {
                sw.Write(dance + ";" + context.Heats[dance].Count);
                foreach (var heat in context.Heats[dance])
                {
                    sw.Write(";" + heat.Couples.Count);
                    foreach (var couple in heat.Couples)
                    {
                        sw.Write(";" + couple.Id);
                    }
                }
                sw.WriteLine("");
            }

            sw.Close();
        }

        /// <summary>
        /// Writes the judges drawing.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="path">The path.</param>
        public void WriteJudgesDrawing(DataContextClass context, string path)
        {
            if (context.JudgeDrawing == null) return;

            var sw = new StreamWriter(path);
            
            foreach (var dance in context.Heats.Keys)
            {
                var list = context.JudgeDrawing[dance].OrderBy(o => o.Heat.Id).ToList();
                sw.Write(String.Format("{0};{1}", dance, list.Count));
                foreach (var judgesDrawing in list)
                {
                    sw.Write(String.Format(";{0};{1};{2}", judgesDrawing.Heat.Id, judgesDrawing.Area, judgesDrawing.Judges.Count));
                    foreach (var judge in judgesDrawing.Judges)
                    {
                        sw.Write(";" + judge.Sign);
                    }
                }
                sw.WriteLine("");
            }
            sw.Close();
        }
    }
}
