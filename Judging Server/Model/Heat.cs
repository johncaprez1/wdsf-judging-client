﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    public class Heat
    {
        public int Id { get; set; }
        
        public Dance Dance { get; set; }
        
        public List<Couple> Couples { get; set; }
        
        public string CouplesList { 
            get { 
                if (Couples.Count == 0) return "";
                var ret = Couples[0].Id.ToString();
                var index = 1;
                while (index < Couples.Count)
                {
                    ret += ", " + Couples[index].Id;
                    index++;
                }

                return ret;
            } 
        
        }

        /// <summary>
        /// Gets the Heat-List to diplay in the drop down to select the heat.
        /// </summary>
        /// <value>
        /// The display.
        /// </value>
        public string Display
        {
            get
            {
                return string.Format("{0} ({1}", Dance.LongName, CouplesList);
            }
        }
     
    }
}
