namespace Judging_Server.Model
{
    public class TotalPerCouple : System.ComponentModel.INotifyPropertyChanged
    {
        private double _total;
        
        private int _state;

        public double Total {
            get { return this._total; }
            set
            {
                this._total = value; 
                this.RaiseEvent("Total");
                this.RaiseEvent("TotalAndArea");
            }
        }

        public int State { 
            get { return this._state; }
            set
            {
                this._state = value; 
                this.RaiseEvent("State");
            } 
        }

        public string Area { get; set; }

        public string Judge { get; set; }
        
        public int Couple {get; set; }

        public string TotalAndArea
        {
            get
            {
                return string.Format("{0:0.0} ({1})", this.Total, this.Area);
            }
        }


        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (this.PropertyChanged != null && DataContextClass.RaiseEvents)
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}