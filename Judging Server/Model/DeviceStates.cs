using System;

namespace Judging_Server.Model
{
    public class DeviceStates : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Position of Device, used for Formation EventsS
        /// </summary>
        public string Position { get; set; }

        public string DeviceId { get; set; }

        private int _dance;

        public int Dance
        {
            get { return this._dance; }
            set { this._dance = value;
                this.RaiseEvent("Dance");    
            }
        }

        private int _heat;
        public int Heat
        {
            get { return this._heat; }
            set { this._heat = value;
                this.RaiseEvent("Heat");
            }
        }
        private string _state;

        public string State
        {
            get { return this._state; }
            set { this._state = value;
                this.RaiseEvent("State");
            }
        }

        private DateTime _time;
        public DateTime Time
        {
            get { return this._time; }
            set { this._time = value; this.RaiseEvent("Time"); }
        }

        private double _mark;
        public double Area
        {
            get { return this._mark; }
            set { this._mark = value; this.RaiseEvent("Area"); }
        }

        public string DevicePath { get; set; }

        // Sign of assigned Judge
        public string Judge { get; set; }

        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (this.PropertyChanged != null && DataContextClass.RaiseEvents)
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}