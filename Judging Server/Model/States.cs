namespace Judging_Server.Model
{
    public enum States
    {
        Used = 1,
        NotUsedMinimum = 2,
        NotUsedMaximum = 3,
        NotUsed = 4
    }
}