﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    public abstract class DanceResultBase : System.ComponentModel.INotifyPropertyChanged
    {
        protected List<TotalPerCouple> _couplesTotal;
        protected Dictionary<string, TotalPerCouple> _lookUpTotalsPerCouple;
        protected Dictionary<string, Judgements> _judgements;
        protected double _chairmanReduction;
        protected List<JudgingArea> _judgmentAreas;
 
        public DanceResultBase(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges)
        {
            _judgmentAreas = judgingAreas;
     
            Dance = dance;
            Couple = couple;
            _couplesTotal = new List<TotalPerCouple>();
            _lookUpTotalsPerCouple = new Dictionary<string, TotalPerCouple>();
            _judgements = new Dictionary<string, Judgements>();
            for (int i = 0; i < judges.Count; i++)
            {
                var c = new TotalPerCouple() { Couple = couple.Id, Judge = judges[i].Sign, State = (int)States.Used, Total = 0 };
                _couplesTotal.Add(c);
                _lookUpTotalsPerCouple.Add(judges[i].Sign, c);
            }
        }

        public string Dance { get; set; }

        public Couple Couple { get; set; }

        protected int _place;

        public int Place
        {
            get { return _place; }
            set
            {
                _place = value;
                RaiseEvent("Place");
            }
        }

        protected void TotalJudgeSetter(string judge, Judgements value)
        {
            _lookUpTotalsPerCouple[judge].Total = value.MarkA + value.MarkB + value.MarkC + value.MarkD + value.MarkE;
            if (_judgements.ContainsKey(judge))
            {
                _judgements[judge] = value;
            }
            else
            {
                _judgements.Add(judge, value);
            }
            CalculateTotal();
        }

        public double TotalJudgeGetter(string judge)
        {
            if (_lookUpTotalsPerCouple.ContainsKey(judge))
                return _lookUpTotalsPerCouple[judge].Total;

            return 0;
        }

        public int StateofJudge(string judge)
        {
            if (_lookUpTotalsPerCouple.ContainsKey(judge))
                return _lookUpTotalsPerCouple[judge].State;

           return 0;
        }

        protected double _total;
        /// <summary>
        /// Grand Total of all Judges but without best and worst judgement
        /// </summary>
        public double Total
        {
            get { return _total; }
            set
            {
                _total = value;
                RaiseEvent("Total");
            }
        }

        public double ChairmanReduction
        {
            get { return _chairmanReduction; }
            set
            {
                _chairmanReduction = value;
                CalculateTotal();
                RaiseEvent("ChairmanReduction");
            }
        }

        public Dictionary<string, TotalPerCouple> ResultsPerJudge
        {
            get { return _lookUpTotalsPerCouple; }
        }

        public double Total_Judge_A
        {
            get { return TotalJudgeGetter("A"); }
            // set { TotalJudgeSetter("A", value); }
        }
       
        protected double _result_A;

        public double Result_A
        { get { return _result_A * (_judgmentAreas.Count > 0 ? _judgmentAreas[0].Weight : 0); } 
            set
            {
                _result_A = value;
                RaiseEvent("Result_A");
            }
        }

        protected double _result_B;
        public double Result_B
        {
            get { return _result_B * (_judgmentAreas.Count > 1 ? _judgmentAreas[1].Weight : 0); }
            set
            {
                _result_B = value;
                RaiseEvent("Result_B");
            }
        }

        protected double _result_C;
        public double Result_C
        {
            get { return _result_C * (_judgmentAreas.Count > 2 ? _judgmentAreas[2].Weight : 0); }
            set
            {
                _result_C = value;
                RaiseEvent("Result_C");
            }
        }

        protected double _result_D;
        public double Result_D
        {
            get { return _result_D * (_judgmentAreas.Count > 3 ? _judgmentAreas[3].Weight : 0); }
            set
            {
                _result_D = value;
                RaiseEvent("Result_D");
            }
        }

        protected double _result_E;
        public double Result_E
        {
            get { return _result_E * (_judgmentAreas.Count > 4 ? _judgmentAreas[4].Weight : 0); }
            set
            {
                _result_E = value;
                RaiseEvent("Result_E");
            }
        }


        public void UpdateByJudge(string judge, Judgements judgements)
        {
            var marks = new List<double>()
                {
                    judgements.MarkA,
                    judgements.MarkB,
                    judgements.MarkC,
                    judgements.MarkD,
                    judgements.MarkE
                };

            if (judgements.MarkA > -1) _lookUpTotalsPerCouple[judge].Area = "TQ";
            if (judgements.MarkB > -1) _lookUpTotalsPerCouple[judge].Area = "MM";
            if (judgements.MarkC > -1) _lookUpTotalsPerCouple[judge].Area = "PS";
            if (judgements.MarkD > -1) _lookUpTotalsPerCouple[judge].Area = "CP";

            _lookUpTotalsPerCouple[judge].Total = marks.Where(m => m > -1). Sum();

            if (_judgements.ContainsKey(judge))
            {
                _judgements[judge] = judgements;
            }
            else
            {
                _judgements.Add(judge, judgements);
            }

            // Nun müssen wir noch die Gesamtsumme Total über alle Tänze neu berechnen
            CalculateTotal();
            RaiseEvent("ResultsPerJudge");
        }


        protected abstract void CalculateTotal();
        
        
        #region INotifyPropertyChanged Members

        protected void RaiseEvent(string Property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(Property));
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion
    }

}
