namespace Judging_Server.Model
{
    public class JudgingArea
    {
        public string Component { get; set; }
        public string Description { get; set; }
        public double Weight {get; set;}
    }
}