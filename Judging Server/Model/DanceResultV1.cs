﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Judging_Server.Model
{
    /// <summary>
    /// DanceResult hält für ein Paar das Ergebnis für einen Tanz. Hierbei wird jeweils das beste und schlechtese Ergebnis der WR's
    /// gelöscht. Welche dies sind, wird im Status gespeichert, so dass dieser entrsprechend nicht angezeigt werden muss (oder anders)
    /// </summary>
    public class DanceResultV1 : DanceResultBase
    {
       
        public DanceResultV1(List<JudgingArea> judgingAreas, Couple couple, string dance, List<Judge> judges) : base(judgingAreas, couple, dance, judges)
        {
            
        }

        protected override void CalculateTotal()
        {
            foreach (TotalPerCouple p in _couplesTotal)
                p.State = (int)States.Used;

            // Regel: Aus jedem der 5 Wertungsbereiche wird jeweils der höchste und
            // niedrigste gestrichen 
            var minA = _judgements.Min(j => j.Value.MarkA);
            var minB = _judgements.Min(j => j.Value.MarkB);
            var minC = _judgements.Min(j => j.Value.MarkC);
            var minD = _judgements.Min(j => j.Value.MarkD);
            var minE = _judgements.Min(j => j.Value.MarkE);

            var maxA = _judgements.Max(j => j.Value.MarkA);
            var maxB = _judgements.Max(j => j.Value.MarkB);
            var maxC = _judgements.Max(j => j.Value.MarkC);
            var maxD = _judgements.Max(j => j.Value.MarkD);
            var maxE = _judgements.Max(j => j.Value.MarkE);
            // wir sortieren die Liste Total 
            _couplesTotal.Sort(new TotalPerCoupleSorter());
            _couplesTotal[0].State = (int)States.NotUsedMinimum;
            _couplesTotal[_couplesTotal.Count - 1].State = (int)States.NotUsedMaximum;
            // Sicherheitshalber für alle States einen Changes Event raisen
            RaiseEvent("State_Judge_A");
            RaiseEvent("State_Judge_B");
            RaiseEvent("State_Judge_C");
            RaiseEvent("State_Judge_D");
            RaiseEvent("State_Judge_E");
            RaiseEvent("State_Judge_F");
            RaiseEvent("State_Judge_G");
            RaiseEvent("State_Judge_H");
            RaiseEvent("State_Judge_I");
            RaiseEvent("State_Judge_J");
            RaiseEvent("State_Judge_K");
            RaiseEvent("State_Judge_L");
            RaiseEvent("State_Judge_M");

            RaiseEvent("Marking_A");

            double sumA = 0;
            double sumB = 0;
            double sumC = 0;
            double sumD = 0;
            double sumE = 0;

            double numJudges = 0;
            foreach (var judgement in _judgements.Values)
            {
                sumA += judgement.MarkA;
                sumB += judgement.MarkB;
                sumC += judgement.MarkC;
                sumD += judgement.MarkD;
                sumE += judgement.MarkE;
                numJudges++;
            }
            sumA = sumA - minA - maxA;
            sumB = sumB - minB - maxB;
            sumC = sumC - minC - maxC;
            sumD = sumD - minD - maxD;
            sumE = sumE - minE - maxE;
            // Mittelwert auf die Anzahl der Judges
            sumA /= (numJudges - 2);
            sumB /= (numJudges - 2);
            sumC /= (numJudges - 2);
            sumD /= (numJudges - 2);
            sumE /= (numJudges - 2);
            _result_A = sumA;
            _result_B = sumB;
            _result_C = sumC;
            _result_D = sumD;
            _result_E = sumE;
            // We add chairman reductions (if applied) and
            // calculate the sum based on the weight of each component
            double sum = sumA * (_judgmentAreas.Count > 0 ? _judgmentAreas[0].Weight : 1) +
                         sumB * (_judgmentAreas.Count > 1 ? _judgmentAreas[1].Weight : 1) +
                         sumC * (_judgmentAreas.Count > 2 ? _judgmentAreas[2].Weight : 1) +
                         sumD * (_judgmentAreas.Count > 3 ? _judgmentAreas[3].Weight : 1) +
                         sumE * (_judgmentAreas.Count > 4 ? _judgmentAreas[4].Weight : 1) +
                         ChairmanReduction;

            sum = Math.Round(sum, 2);
            if (sum != Total)
            {
                Total = sum;
                RaiseEvent("Total");
            }
        }

    }

}
