﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;

using Judging_Server.Model;
using Judging_Server.Print.Controls;

using Scrutinus.Reports.BasicPrinting;

namespace Judging_Server.Print
{
    class CoupleDetailResultPrinter : AbstractPrinter
    {
        private DataContextClass context;

        private FixedPage page;

        public CoupleDetailResultPrinter(DataContextClass context)
        {
            this.context = context;
            this.CreateContent();
        }

        protected override void CreateCustomContent()
        {
            this.page = this.CreatePage();

            foreach (var couple in context.Couples)
            {
                var control = new PrintResultByComponents(
                    couple,
                    context.JudgingAreas,
                    context.AllJudgements,
                    context.Dances);

                control.Width = DotsFromCm(this.PageWidth - this.LeftPageMargin - this.RightPageMargin);

                this.page = this.AddControlToPage(page, control);

                this.AddHorizontalLine(
                    page,
                    CmFromDots(this.HeaderHeigthInDots + 10),
                    this.LeftPageMargin,
                    this.PageWidth - this.LeftPageMargin - this.RightPageMargin);

                this.HeaderHeigthInDots += 20;
            }
        }

        protected override void AddCommenContent(System.Windows.Documents.FixedPage page)
        {
            throw new NotImplementedException();
        }
    }
}
