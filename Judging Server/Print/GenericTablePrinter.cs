﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

using Scrutinus.Reports.BasicPrinting.Controls;

namespace Scrutinus.Reports.BasicPrinting
{
    public class GenericTablePrinter<T> : AbstractPrinter
    {
        private Grid grid;

        protected IList<T> dataList;

        protected IList<ColumnDescriptor<T>> columns;

        protected FixedPage currentPage;

        private int currentRowNumber;

        public GenericTablePrinter()
            : base()
        {
            this.DoCreateHeader = false;
            this.TableFontSize = 12;
        }

        public bool DoCreateHeader { get; set; }

        public double TableFontSize { get; set; }

        public void CreateReport()
        {
            this.CreateContent();
        }

        private void CreateColumns()
        {
            foreach (ColumnDescriptor<T> t in this.columns)
            {
                this.grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = t.Width });
            }
        }

        private void CreateHeader()
        {
            this.grid.RowDefinitions.Add(new RowDefinition());

            for (var index = 0; index < this.columns.Count; index++)
            {
                var border = new Border()
                                 {
                                     Background = new SolidColorBrush(Colors.LightGray),
                                     Height = 30,
                                     BorderBrush = new SolidColorBrush(Colors.Transparent)
                                 };
                border.Child = new TextBlock()
                                   {
                                       Text = this.columns[index].Header,
                                       Margin = new Thickness(4, 0, 0, 0),
                                       VerticalAlignment = VerticalAlignment.Center,
                                       FontSize = this.TableFontSize,
                                   };
                this.grid.SetCellUiElement(border, index, 0);
            }
        }

        protected FixedPage PrintTable(FixedPage page)
        {
            if (dataList == null)
            {
                return page;
            }

            var index = 0;

            FixedPage newPage = null;

            if (this.dataList.Count == 0)
            {
                this.CreateGrid(index, page, out newPage);
                return newPage;
            }

            while (index < this.dataList.Count)
            {
                index = this.CreateGrid(index, page, out newPage);
                page = newPage;
            }

            return newPage;
        }

        protected override void CreateCustomContent()
        {
            this.PrintTable(this.CreatePage());
        }

        private int CreateGrid(int startIndex, FixedPage currentPage, out FixedPage page)
        {
            if (currentPage == null)
            {
                page = this.CreatePage();
            }
            else
            {
                page = currentPage;
            }

            this.grid = new Grid() { Width = this.UseablePageWidthInDots };

            FixedPage.SetLeft(this.grid, DotsFromCm(this.LeftPageMargin)); // left margin
            FixedPage.SetTop(this.grid, this.HeaderHeigthInDots); // top margin

            this.CreateColumns();

            page = this.AddControlToPage(page, grid);

            this.currentRowNumber = 0;

            if (this.DoCreateHeader)
            {
                this.CreateHeader();
                this.currentRowNumber = 1;
            }

            while (startIndex < this.dataList.Count && this.ControlFitsOnPage(page, grid))
            {
                this.AddDataRow(startIndex);
                startIndex++;
            }

            if (startIndex < this.dataList.Count)
            {
                // grid to heigh -> remove last added line + children of this line:
                var uIElements =
                    this.grid.Children.Cast<UIElement>()
                        .Where(e => Grid.GetRow(e) == this.currentRowNumber - 1)
                        .ToList();
                foreach (var uiElement in uIElements)
                {
                    this.grid.Children.Remove(uiElement);
                }
                this.grid.RowDefinitions.RemoveAt(this.grid.RowDefinitions.Count - 1);
                startIndex--;
                page = this.CreatePage();

                return startIndex;
            }

            this.MeasurePage(page);
            this.HeaderHeigthInDots += this.grid.ActualHeight;

            return startIndex;
        }

        private void AddDataRow(int index)
        {
            this.grid.RowDefinitions.Add(new RowDefinition());

            for (int column = 0; column < this.columns.Count; column++)
            {
                var text = new TextBlock()
                               {
                                   Text = columns[column].ValueFunc(dataList[index]) != null ? columns[column].ValueFunc(dataList[index]).ToString() : "",
                                   Margin = new Thickness(4),
                                   TextWrapping = TextWrapping.WrapWithOverflow,
                                   FontSize = this.TableFontSize,
                               };
                this.grid.SetCellUiElement(text, column, this.currentRowNumber);
            }

            this.currentRowNumber++;
        }
     
        protected override void AddCommenContent(FixedPage page)
        {
            // Add copyright at end of page ..
            
        }
    }
}