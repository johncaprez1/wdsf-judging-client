﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Judging_Server.Model;
using Heat = Judging_Server.ReportModels.Heat;

namespace Judging_Server.Dialogs
{

    public class CreateHeatsModel
    {
        public bool PrintDances { get; set; }
        public bool PrintCouples { get; set; }
        public int NumberHeats { get; set; }
        public int CopiesDances { get; set; }
        public int CopiesCouples { get; set; }
        public bool IsFinal { get; set; }
    }

    /// <summary>
    /// Interaction logic for CreateHeats.xaml
    /// </summary>
    public partial class CreateHeats : Window
    {

        private CreateHeatsModel _model;
        private Model.DataContextClass _context;

        public CreateHeats(Model.DataContextClass context)
        {
            _model = new CreateHeatsModel()
                {
                    CopiesCouples = 1,
                    CopiesDances = 3,
                    NumberHeats = 3,
                    PrintCouples = true,
                    PrintDances = true,
                };
            
            InitializeComponent();
            _context = context;
            DataContext = _model;
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            // Create heats:
            var coupleDrawer = new Helper.CreateRoundDrawing();
            var judgeDrawer = new Helper.CreateJudgesDrawing();

            _context.Heats = _model.IsFinal ? coupleDrawer.CreateFinalDrawing(_context.Couples, _context.Dances) : 
                                              coupleDrawer.CreateDrawing(_context.Couples, _context.Dances, _model.NumberHeats);
            if (_context.IsFormation)
            {
                _context.JudgeDrawing = judgeDrawer.CreateFormation(_context.JudgingAreas, _context.Judges, _context.Heats, 3);
            }
            else
            {
                if(_context.CalculationVersion != CalculationVersionEnum.Version1)
                    _context.JudgeDrawing = judgeDrawer.Create(_context.JudgingAreas, _context.Judges, _context.Heats, 3, 4);
            }
            var exportHelper = new Helper.ExportHelpers();
            exportHelper.WriteHeatData(_context, ServerSettings.Default.DataPath + String.Format("\\{0}_{1}\\heats.txt", _context.Event.ScrutinusId, _context.Event.RoundNr));
            exportHelper.WriteJudgesDrawing(_context, ServerSettings.Default.DataPath +  String.Format("\\{0}_{1}\\judgesDrawing.txt", _context.Event.ScrutinusId, _context.Event.RoundNr));
            // Now we need to print
            if (_model.PrintDances)
            {
                Print.Printer.PrintReport(_context.Event.Title, "Drawing Grand Slam",
                                          "Judging_Server.Print.RoundDrawingNormal.rdlc", ReportModels.ReportModelHelper.getDrawingDataSource(_context),
                                          "HeatsDataSet", _model.CopiesDances);
            }

            if (_model.PrintCouples)
            {
                Print.Printer.PrintReport(_context.Event.Title, "Drawing Grand Slam",
                                          "Judging_Server.Print.RoundDrawingNormal.rdlc", ReportModels.ReportModelHelper.getDrawingDataSource(_context),
                                          "HeatsDataSet", _model.CopiesDances);
            }
            // And we are done ...
            this.DialogResult = true;
            Close();
        }


        private void NumHeats_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            if (NumHeats == null)
                return; 

            int res = 0;
            if (Int32.TryParse(NumHeats.Text, out res))
            {
                var couples = _context.Couples.Count/res;
                if (_context.Couples.Count%res > 0) couples++;

                LblTextHeats.Content = String.Format("{0} Couples per Heat (max)", couples);
            }
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
