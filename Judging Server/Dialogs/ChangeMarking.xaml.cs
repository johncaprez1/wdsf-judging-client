﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Judging_Server.Dialogs
{
    /// <summary>
    /// Interaction logic for ChangeMarking.xaml
    /// </summary>
    public partial class ChangeMarking : Window
    {
        private Model.DataContextClass _context;

        public ChangeMarking(Model.DataContextClass context)
        {
            InitializeComponent();

            Judges.ItemsSource = context.Judges;
            Dances.ItemsSource = context.Dances;
            _context = context;
        }

        private void BtnOK_OnClick(object sender, RoutedEventArgs e)
        {
            if (Judges.SelectedItem == null || Dances.SelectedItem == null) return;

            var judge = (Model.Judge) Judges.SelectedItem;
            var dance = (Model.Dance) Dances.SelectedItem;

            var device = _context.DeviceState.Single(d => d.Judge == judge.Sign);
            var path =  device.DevicePath + "\\newResult.txt";

            try
            {
                var inStr = new StreamWriter(path);
                inStr.WriteLine("{0};{1};{2}", dance.ShortName, judge.Sign, NewMarking.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            Close();
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
