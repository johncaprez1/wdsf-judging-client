﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Printing;
using System.Security.AccessControl;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Judging_Server.Model;
using Judging_Server.ReportModels;

namespace Judging_Server.Dialogs
{
    public class ShowBestWorstAnalysisViewModel : INotifyPropertyChanged
    {
        public ICommand PrintCommand { get; set; }
        private IEnumerable<BestWorstAnalysisPrintModel> bestWorstAnalysis;
        public IEnumerable<BestWorstAnalysisPrintModel> BestWorstAnalysis {
            get
            {
                return bestWorstAnalysis;
            } 
            
            set
            {
                bestWorstAnalysis = value; 
                RaiseEvent("BestWorstAnalysis");
            } 


        }

        public ShowBestWorstAnalysisViewModel()
        {
            var IsInDesignMode = true;
            if (IsInDesignMode)
            {
                BestWorstAnalysis = new List<BestWorstAnalysisPrintModel>();

                var bw = new BestWorstAnalysisPrintModel()
                {
                    BestWorstAreaA =
                        new JudgeAreaViewModel()
                        {
                            Best = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Worst = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Media = new JudgeBestWorsPrintModel() {DistanceMedia = 1, IsToFar = true, JudgeSign = "A"}
                        },
                    BestWorstAreaB =
                        new JudgeAreaViewModel()
                        {
                            Best = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Worst = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Media = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" }
                        },
                    BestWorstAreaC =
                        new JudgeAreaViewModel()
                        {
                            Best = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Worst = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Media = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" }
                        },
                    BestWorstAreaD =
                        new JudgeAreaViewModel()
                        {
                            Best = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Worst = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" },
                            Media = new JudgeBestWorsPrintModel() { DistanceMedia = 1, IsToFar = true, JudgeSign = "A" }
                        }
                };

                var list = BestWorstAnalysis as List<BestWorstAnalysisPrintModel>;
                list.Add(bw);
            }
        }

        public void Export()
        {
            var filename = ServerSettings.Default.ResultExportPaths + "//exportBestWorstAnalysis.csv";

            var file = new StreamWriter(filename);

            file.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                "Number", "Couple", "Dance", 
                "Worst A", "J. A", "Best A", "J. A",
                "Worst B", "J. B", "Best B", "J. B",
                "Worst C", "J. C", "Best C", "J. C",
                "Worst D", "J. D", "Best D", "J. D"
                );

            foreach (var model in this.BestWorstAnalysis)
            {
                file.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18}",
                    model.Number, model.Number,model.Dance,
                    model.BestWorstAreaA.Worst.DistanceMedia, model.BestWorstAreaA.Worst.JudgeSign, model.BestWorstAreaA.Best.DistanceMedia, model.BestWorstAreaA.Best.JudgeSign,
                    model.BestWorstAreaB.Worst.DistanceMedia, model.BestWorstAreaB.Worst.JudgeSign, model.BestWorstAreaB.Best.DistanceMedia, model.BestWorstAreaB.Best.JudgeSign,
                    model.BestWorstAreaC.Worst.DistanceMedia, model.BestWorstAreaC.Worst.JudgeSign, model.BestWorstAreaC.Best.DistanceMedia, model.BestWorstAreaC.Best.JudgeSign,
                    model.BestWorstAreaD.Worst.DistanceMedia, model.BestWorstAreaD.Worst.JudgeSign, model.BestWorstAreaD.Best.DistanceMedia, model.BestWorstAreaD.Best.JudgeSign
                    );
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    /// <summary>
    /// Interaction logic for ShowBestWorstAnalysis.xaml
    /// </summary>
    public partial class ShowBestWorstAnalysis : Window
    {
        private ShowBestWorstAnalysisViewModel viewModel;
        private DataContextClass dataContext;

        public ShowBestWorstAnalysis(DataContextClass DataContext)
        {
            InitializeComponent();

            dataContext = DataContext;

            viewModel = new ShowBestWorstAnalysisViewModel
            {
                BestWorstAnalysis = DataContext.CreateBestWorstPrintModel()
            };

            this.DataContext = viewModel;
        }

        private void ShowBestWorstAnalysis_OnGotFocus(object sender, RoutedEventArgs e)
        {
           
        }

        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T;
                if (child == null)
                {
                    child = GetVisualChild<T>(v);
                }
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        public DataGridRow GetSelectedRow(DataGrid grid)
        {
            return (DataGridRow)grid.ItemContainerGenerator.ContainerFromItem(grid.SelectedItem);
        }

        public DataGridRow GetRow(DataGrid grid, int index)
        {
            DataGridRow row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);

            return row;
        }

        public DataGridCell GetCell(DataGrid grid, DataGridRow row, int column)
        {
            if (row != null)
            {
                DataGridCellsPresenter presenter = GetVisualChild<DataGridCellsPresenter>(row);

                if (presenter == null)
                {
                    grid.ScrollIntoView(row, grid.Columns[column]);
                    presenter = GetVisualChild<DataGridCellsPresenter>(row);
                }

                DataGridCell cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(column);
                return cell;
            }
            return null;
        }

        public DataGridCell GetCell(DataGrid grid, int row, int column)
        {
            DataGridRow rowContainer = GetRow(ResultDataGrid, row);
            return GetCell(grid, rowContainer, column);
        }

        
        private void Reload_OnClick(object sender, RoutedEventArgs e)
        {
            viewModel.BestWorstAnalysis = dataContext.CreateBestWorstPrintModel();
        }

        private void ResultDataGrid_OnBeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            e.Cancel = true;
        }

        private void PrintVisual(Visual v)
        {

            System.Windows.FrameworkElement e = v as System.Windows.FrameworkElement;
            if (e == null)
                return;

            PrintDialog pd = new PrintDialog();
            if (pd.ShowDialog() == true)
            {
                pd.PrintTicket.PageOrientation = PageOrientation.Landscape;
                //store original scale
                Transform originalScale = e.LayoutTransform;
                //get selected printer capabilities
                System.Printing.PrintCapabilities capabilities = pd.PrintQueue.GetPrintCapabilities(pd.PrintTicket);

                //get scale of the print wrt to screen of WPF visual
                double scale = Math.Min(capabilities.PageImageableArea.ExtentWidth / e.ActualWidth, capabilities.PageImageableArea.ExtentHeight /
                               e.ActualHeight);
                scale = .5;
                //Transform the Visual to scale
                e.LayoutTransform = new ScaleTransform(scale, scale);

                //get the size of the printer page
                System.Windows.Size sz = new System.Windows.Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);

                //update the layout of the visual to the printer page size.
                e.Measure(sz);
                e.Arrange(new System.Windows.Rect(new System.Windows.Point(capabilities.PageImageableArea.OriginWidth, capabilities.PageImageableArea.OriginHeight), sz));

                //now print the visual to printer to fit on the one page.
                pd.PrintVisual(v, "My Print");

                //apply the original transform.
                e.LayoutTransform = originalScale;
            }
        }

        private void Print_OnClick(object sender, RoutedEventArgs e)
        {
            PrintVisual(ResultDataGrid);
        }

        private void Export_OnClick(object sender, RoutedEventArgs e)
        {
            viewModel.Export();
        }
    }
}
