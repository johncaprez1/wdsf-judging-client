﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace TVGraphics
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private FileSystemWatcher _watcher;
        private string _lastFile;
        private DateTime _lastEvent;

        public MainWindow()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            InitializeComponent();

            _watcher = new FileSystemWatcher(TVGraphicsSettings.Default.Path, "*.txt");
            _watcher.Created += new FileSystemEventHandler(FileSystemWatcherChanged);
            _watcher.Changed += new FileSystemEventHandler(FileSystemWatcherChanged);
            _watcher.EnableRaisingEvents = true;

            MainContent.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;

            

            if (TVGraphicsSettings.Default.TVOutput)
            {
                this.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
                this.Width = 1280;
                this.Height = 1024;
                this.Left = 1366;
                this.Top = 0;
                
            }
            else
            {
                this.Width = 1024;
                this.Height = 768;
            }

            _lastFile = "";
            _lastEvent = DateTime.Now;
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is Exception)
            {
                var ex = (Exception) e.ExceptionObject;
                MessageBox.Show(ex.Message);
                if (ex.InnerException != null)
                {
                    MessageBox.Show(ex.InnerException.Message);
                }
            }
        }

        public void HandleInvoced(string filename, string FullPath)
        {
            this.WindowStyle = WindowStyle.None;
            this.WindowState = System.Windows.WindowState.Maximized;
            
            switch (filename)
            {
                case "DanceResult.txt":
                    ShowDanceResult(FullPath);
                    break;
                case "DanceTotal.txt":
                    ShowDanceTotal(FullPath);
                    break;
                case "TotalResult.txt":
                    ShowTotalResult(FullPath);
                    break;
                case "ClearResult.txt":
                    ShowBlank();
                    break;
                case "PlaceAndTitle.txt":
                    ShowPlaceAndTitle(FullPath);
                    break;
            }
        }

        private void ShowPlaceAndTitle(string file)
        {
            string[] lines = LoadDataFile(file);
            string[] data;
            var list = new List<Models.ListResultElement>();

            var title = lines[0];


            for (int i = 1; i < lines.Length; i++)
            {
                data = lines[i].Split(';');
                var element = new Models.ListResultElement();
                list.Add(element);
                element.Place = Int32.Parse(data[0]);
                element.Sum = 0;
                element.Country = data[2];
                var couple = data[1].Split('/');
                element.NameMan = couple[0].Trim();
                element.NameWoman = couple[1].Trim();
            }

            System.Windows.Controls.Page page;

            if (!TVGraphicsSettings.Default.TVOutput)
            {
                page = new ResultPage(title, list);
            }
            else
            {
                page = new ResultPageTV(title, list);
            }

            page.ShowsNavigationUI = false;
            this.MainContent.Navigate(page);

            if (TVGraphicsSettings.Default.TVOutput)
            {
                var tv = (ResultPageTV)page;
                Dispatcher.Invoke(
                    DispatcherPriority.Normal,
                    (Action)(() => { tv.ShowResult(); })
                    );
                ;
            }
        }

        void FileSystemWatcherChanged(object sender, FileSystemEventArgs e)
        {
            if (_lastFile == e.FullPath)
            {
                var dif = DateTime.Now.Ticks - _lastEvent.Ticks;
                if (dif < 200000)
                    return;
            }
            _lastFile = e.FullPath;
            _lastEvent = DateTime.Now;
            // Abhängig von der Datei lesen wir nun ein, was angezeigt werden soll
            var task = new Task(() =>
            {
                Dispatcher.Invoke(
                    DispatcherPriority.Normal,
                    (Action)(() => { HandleInvoced(e.Name, e.FullPath); })
                    );
            });
            task.Start();
        }

        string[] LoadDataFile(string file)
        {
            string[] res = null;
            int count = 0;

            while (res == null && count < 20)
            {
                try
                {
                    res = File.ReadAllLines(file, Encoding.UTF8);
                    if (res.Length == 0)
                    {
                        res = null;
                        throw new Exception("No data");
                    }
                    return res;
                }
                catch (Exception)
                {
                    count++;
                    Thread.Sleep(300);
                }
            }
            return new string[1];
        }

        void ShowBlank()
        {
            var page = new ClearPage();
            page.ShowsNavigationUI = false;
            this.MainContent.Navigate(page);
        }

        void ShowDanceResult(string file)
        {
            System.Globalization.CultureInfo de = new System.Globalization.CultureInfo("de-DE");
            string[] lines = LoadDataFile(file);

            var marks  = lines[3].Split(';');
            var couple = lines[2].Split(';');
            var name = couple[2].Split('/');
            var results = lines[4].Split(';');

            var model = new Models.CoupleResultModel()
            {
                MarkA = double.Parse(marks[0],de),
                MarkB = double.Parse(marks[1], de),
                MarkC = double.Parse(marks[2], de),
                MarkD = double.Parse(marks[3], de),
                MarkE = double.Parse(marks[4], de),
                NameMan = name[0].Trim().ToUpper(),
                NameWoman = name[1].Trim().ToUpper(),
                Country = couple[3],
                Place = results[1] + ".",
                Sum = double.Parse(results[0], de)
            };

            System.Windows.Controls.Page page;
            if (!TVGraphicsSettings.Default.TVOutput)
            {
                page = new CoupleResult(model);
            }
            else
            {
                page = new CoupleResultTV(model);
            }

            page.ShowsNavigationUI = false;
            this.MainContent.Navigate(page);
        }

        void ShowDanceTotal(string file)
        {
            System.Globalization.CultureInfo de = new System.Globalization.CultureInfo("de-DE");

            string[] lines = LoadDataFile(file);
            string[] data;
            var list = new List<Models.ListResultElement>();

            var titles = lines[0].Split(';');


            for (int i = 1; i < lines.Length; i++)
            {
                data = lines[i].Split(';');
                var element = new Models.ListResultElement();
                list.Add(element);
                element.Place = Int32.Parse(data[0]);
                element.Sum = double.Parse(data[1],de);
                element.Country = data[4];
                var couple = data[3].Split('/');
                element.NameMan = couple[0].Trim();
                element.NameWoman = couple[1].Trim();
            }

            System.Windows.Controls.Page page;

            if (!TVGraphicsSettings.Default.TVOutput)
            {
                page = new ResultPage(titles[1], list);
            }
            else
            {
                page = new ResultPageTV(titles[1], list);
            }

            page.ShowsNavigationUI = false;
            this.MainContent.Navigate(page);
            if (TVGraphicsSettings.Default.TVOutput)
            {
                var tv = (ResultPageTV)page;
                tv.ShowResult();
            }
        }

        void ShowTotalResult(string file)
        {
            System.Globalization.CultureInfo de = new System.Globalization.CultureInfo("de-DE");

            string[] lines = LoadDataFile(file);
            string[] data;
            var list = new List<Models.ListResultElement>();

            var titles = lines[0].Split(';');


            for (int i = 0; i < lines.Length; i++)
            {
                data = lines[i].Split(';');
                var element = new Models.ListResultElement();
                list.Add(element);
                element.Place = Int32.Parse(data[0]);
                element.Sum = double.Parse(data[1],de);
                element.Country = data[4];
                var couple = data[3].Split('/');
                element.NameMan = couple[0].Trim();
                element.NameWoman = couple[1].Trim();
            }

            System.Windows.Controls.Page page;

            if (!TVGraphicsSettings.Default.TVOutput)
            {
                page = new ResultPage("Total Result", list);
            }
            else
            {
                page = new ResultPageTV("Total Result", list);
            }
            
            page.ShowsNavigationUI = false;
            this.MainContent.Navigate(page);

            if (TVGraphicsSettings.Default.TVOutput)
            {
                var tv = (ResultPageTV)page;
                tv.ShowResult();
            }
        }
    }
}
