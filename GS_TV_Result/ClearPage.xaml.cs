﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TVGraphics
{
    /// <summary>
    /// Interaction logic for ClearPage.xaml
    /// </summary>
    public partial class ClearPage : Page
    {
        public ClearPage()
        {
            InitializeComponent();
            if (TVGraphicsSettings.Default.TVOutput)
            {
                BackgroundCanvas.Background = new SolidColorBrush(Colors.Green);
                Width = 1920;
                Height = 1080;
            }
            else
            {
                BackgroundCanvas.Background = new SolidColorBrush(Colors.Black);
                Width = 1024;
                Height = 786;
            }
        }
    }
}
