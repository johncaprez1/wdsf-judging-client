﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TVGraphics.Models
{
    public class ListResultElement
    {
        public double Sum { get; set; }
        public int Place { get; set; }
        public string NameMan { get; set; }
        public string NameWoman { get; set; }
        public string Country { get; set; }
        public int Number { get; set; }
    }
}
