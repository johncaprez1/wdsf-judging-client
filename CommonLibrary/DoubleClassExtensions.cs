﻿using System;
using System.Globalization;
using System.Threading;

namespace CommonLibrary
{
    public static class DoubleHelper
    {
        public static string DoubleString(this double value)
        {
            var decimalDelimiter = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var str = value.ToString().Replace(decimalDelimiter, ",");
            return str;
        }

        public static double ParseString(this string input)
        {
            var decimalDelimiter = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var str = input.Replace(",", decimalDelimiter).Replace(".", decimalDelimiter);
            return double.Parse(str);
        }

        public static double Round2(this double value)
        {
            return Math.Round(value, 2, MidpointRounding.AwayFromZero);
        }

        public static double Round3(this double value)
        {
            return Math.Round(value, 3, MidpointRounding.AwayFromZero);
        }
    }
}
