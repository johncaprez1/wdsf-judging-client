﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

// Model 2013 !
using System.Threading;
using System.Windows;

using CommonLibrary;

using NLog;

namespace Judging_Client.Model
{

    public class Heat : System.ComponentModel.INotifyPropertyChanged
    {
        

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public int Id { get; set; }
        public string DanceName { get; set; }
        public string DanceShortName { get; set; }
        public int DanceNo { get; set; }
        public int Judgment_Area { get; set; }

        public string DisplayName { get; set; }
        

        public List<Judgement> Couples { get; set; } 

    }

    public class DataModel : System.ComponentModel.INotifyPropertyChanged
    {
        private Logger logger = LogManager.GetCurrentClassLogger();

        public string DataPath { get; set; }

        private string _judge;
        public string Judge {
            get { return _judge;}
            set{
                _judge = value;
                RaiseEvent("Judge");
            }
        }

        public string Sign { get; set; }
        public string Country { get; set; }
        public Dictionary<string, string> HelpStrings { get; set; }
        public string[] Components { get; set; }

        private string _event;


        public string Event
        {
          get { return _event; }
          set { _event = value;
                RaiseEvent("Event");
          }
        }

        public bool IsFinished { get; set; }

        // private Heat _currentHeat;
        public Heat CurrentHeat
        {
            get { return Heats.Count > 0 ? Heats[CurrentIndex] : null; }
        }

        private int _currentIndex;

        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if (value >= Heats.Count)
                {
                    // Do not set CurrentJudgements -> Exception
                    return;
                }
                _currentIndex = value;
                RaiseEvent("CurrentIndex");
                RaiseEvent("CurrentHeat");
            }
        }

        public DataModel()
        {
            // Set Default Values;
            // Judgements = new List<Judgement>();
            Judge = "No Judge";
            Event = "No Event";
            Heats = new List<Heat>();
            CurrentIndex = 0;
            IsFinished = false;
            MinimumValue = 7.5;
            MaximumValue = 10;
            Intervall = 0.25;
        }

        public double MaximumValue { get; set; }

        public double MinimumValue { get; set; }

        public double Intervall { get; set; }

        // public List<Judgement> Judgements {get; set;}

        public List<Heat> Heats { get; set; } 

        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        public void LoadData(System.IO.StreamReader sr)
        {
            // Subfolder where to save the data ...
            this.DataPath = sr.ReadLine();
            // Min, Max and Intervall;
            var separator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            var minMaxData = sr.ReadLine().Split(';');
            this.MinimumValue = minMaxData[0].ParseString();
            this.MaximumValue = minMaxData[1].ParseString();
            this.Intervall = minMaxData[2].ParseString();
            // Components...
            Components = sr.ReadLine().Split(';');

            this.logger.Info("Adding Components");

            HelpStrings = new Dictionary<string, string>();
            for (int i = 0; i < Components.Length; i++)
            {
                var data = sr.ReadLine().Split(';');
                data[1] = data[1].Replace(@"\r\n", "\r\n");
                try
                {
                    HelpStrings.Add(data[0], data[1]);
                }
                catch (Exception ex)
                {
                    this.logger.Error("Could not add Helpstring with Value " + data[0] + ", Message: " + ex.Message);
                }
            }

            this.logger.Info("Components Added");

            var judgedata= sr.ReadLine().Split(';');
            Sign = judgedata[0];
            Judge = judgedata[1];
            Country = judgedata[2];
            // Zweite Zeile die Competition
            Event = sr.ReadLine();
            Heats = new List<Heat>();

            this.logger.Info("Reading Heats");

            while (!sr.EndOfStream)
            {
                var data = sr.ReadLine().Split(';');

                var heats = Int32.Parse(data[3]);
                var index = 4;

                for (var heatIndex = 0; heatIndex < heats; heatIndex++)
                {
                    var heat = new Heat()
                        {
                            DanceShortName = data[1],
                            DanceName = data[2],
                            DanceNo = Int32.Parse(data[0]),
                            Couples = new List<Judgement>(),
                            Id = Heats.Count
                        };

                    heat.DisplayName = (heatIndex + 1) + ". Heat";

                    Heats.Add(heat);

                    var numCouples = Int32.Parse(data[index]);
                    heat.Judgment_Area = Int32.Parse(data[index + 1]);

                    index += 2;
                    for (var c = 0; c < numCouples; c++)
                    {
                        var ju = new Model.Judgement() { Couple = data[index],  Mark = 0d };
                        ju.PropertyChanged += Judgement_PropertyChanged;
                        heat.Couples.Add(ju);
                        index++;
                    }
                }
            }

            this.logger.Info("Read Data finished");
        }

        void Judgement_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaiseEvent("Judgement");
        }

        public void WriteResult(System.IO.StreamWriter writer)
        {
            // Wir könnten unsere Netzwerkverbindung verloren haben,
            // Daher schreiben wir zur Sicherheit einfach alle Ergebnisse raus ...
            foreach (var heat in Heats)
            {
                foreach (var judgement in heat.Couples)
                {
                    writer.Write(Sign + ";" + heat.DanceShortName + ";" + judgement.Couple + ";");
                    writer.WriteLine(String.Format("{0};{1}", heat.Judgment_Area, judgement.Mark));
                }
            }  
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler  PropertyChanged;

        #endregion
    }

    public class Judgement : System.ComponentModel.INotifyPropertyChanged
    {

        private double _mark;
        public double Mark
        {
            get { return _mark; }
            set
            {
                _mark = value;
                RaiseEvent("Mark");
            }
        }

        public string Couple { get; set; }

        private void RaiseEvent(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler  PropertyChanged;

        #endregion
}
}
