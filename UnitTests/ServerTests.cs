﻿using System;
using System.Collections.Generic;
using Judging_Server.Model;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class ServerTests
    {
        private Judging_Server.Model.DataContextClass _dataContext;

        [TestInitialize]
        public void FillData()
        {
            _dataContext = new Judging_Server.Model.DataContextClass {Judges = new List<Judge>()};
            _dataContext.Event = new EventData() {Date = "1.1.2013", Organizer = "Test-Organizer", Place = "Hamburg", Title = "Test-Title"};
            // we have 5 dances
            _dataContext.Dances = new List<Dance>
                {
                    new Dance() {LongName = "Slow Walz", ShortName = "SW"},
                    new Dance() {LongName = "Tango", ShortName = "TG"},
                    new Dance() {LongName = "Vinnes Waltz", ShortName = "VW"},
                    new Dance() {LongName = "Slow Fox", ShortName = "SF"},
                    new Dance() {LongName = "Quickstep", ShortName = "QS"}
                };
            // Lets add 12 judges
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "A" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "B" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "C" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "D" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "E" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "F" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "G" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "H" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "I" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "J" });
            _dataContext.Judges.Add(new Judge() { Country = "Germany", Name = "Test", Sign = "K" });
            _dataContext.Judges.Add( new Judge(){Country = "Germany", Name = "Test", Sign = "L" });

            _dataContext.DeviceState = new List<DeviceStates>();
            _dataContext.DeviceState.Add(new DeviceStates(){Judge = "A", Area = 0d, State = "Online"});

            // lets add 6 couples
            _dataContext.Couples = new List<Couple>
                {
                    new Couple() {Id = 101, Country = "Germany", Name = "Test 1"},
                    new Couple() {Id = 102, Country = "Germany", Name = "Test 2"},
                    new Couple() {Id = 103, Country = "Germany", Name = "Test 3"},
                    new Couple() {Id = 104, Country = "Germany", Name = "Test 4"},
                    new Couple() {Id = 105, Country = "Germany", Name = "Test 5"},
                    new Couple() {Id = 106, Country = "Germany", Name = "Test 6"}
                };

            Judging_Server.Helper.IRoundDrawer drawer = new Judging_Server.Helper.CreateRoundDrawing();
            _dataContext.Heats = drawer.CreateDrawing(_dataContext.Couples, _dataContext.Dances, 2); // should be 3 couples each.

            _dataContext.CreateDataModel();
        } 

        [TestMethod]
        public void TestJudgements()
        {
             // We add marking for one couple. To make it simple, Judge A-C marking Component 1, JudgeD-F Component2 .. and so forth
            _dataContext.UpdateJudgement("A", "SW", 101, 1, 8.5,  8.5, -1, -1, -1, -1);
            _dataContext.UpdateJudgement("B", "SW", 101, 1, 8, 8, -1, -1, -1, -1);
            _dataContext.UpdateJudgement("C", "SW", 101, 1, 9, 9, -1, -1, -1, -1);

            var danceResult = _dataContext.DanceResults["SW"];
            var result = danceResult.Single(p => p.Couple.Id == 101);

            Assert.IsTrue(result.Total == 8.5d);

            _dataContext.UpdateJudgement("A", "SW", 102, 1, 8.5, 8.5, -1, -1, -1, -1);
            _dataContext.UpdateJudgement("B", "SW", 102, 1, 8.5, 8.5, -1, -1, -1, -1);
            _dataContext.UpdateJudgement("C", "SW", 102, 1, 9, 9, -1, -1, -1, -1);
            result = _dataContext.DanceResults["SW"].Single(p => p.Couple.Id == 102);
            Assert.IsTrue(result.Total == 8.63d);
        }

        [TestMethod]
        public void TestJudgments2()
        {
            
        }

        [TestMethod]
        public void ExportHeats()
        {
            var helper3 = new Judging_Server.Helper.ExportHelpers();
            var helper = new Judging_Server.Helper.CreateJudgesDrawing();
            var helper2 = new Judging_Server.Helper.CreateRoundDrawing();

            _dataContext.Heats = helper2.CreateDrawing(_dataContext.Couples, _dataContext.Dances, 2);

            _dataContext.JudgeDrawing = helper.Create(_dataContext.JudgingAreas, _dataContext.Judges, _dataContext.Heats, 3, 4);
            helper3.WriteJudgeData(_dataContext, "c:\\temp\\eJudge", "testdraw.txt", _dataContext.DeviceState[0]);
        }

        [TestMethod]
        public void WriteReadJudgeDrawing()
        {
            var helper3 = new Judging_Server.Helper.ExportHelpers();
            var helper = new Judging_Server.Helper.CreateJudgesDrawing();
            var helper2 = new Judging_Server.Helper.CreateRoundDrawing();

            _dataContext.Heats = helper2.CreateDrawing(_dataContext.Couples, _dataContext.Dances, 2);
            _dataContext.JudgeDrawing = helper.Create(_dataContext.JudgingAreas, _dataContext.Judges, _dataContext.Heats, 3, 4);
            helper3.WriteJudgesDrawing(_dataContext, @"c:\temp\drawing1.txt");
            _dataContext.LoadJudgesDrawing(@"c:\temp\drawing1.txt");
            helper3.WriteJudgesDrawing(_dataContext, @"c:\temp\drawing2.txt");
            // compare the two files
            var content1 = System.IO.File.ReadAllText(@"c:\temp\drawing1.txt");
            var content2 = System.IO.File.ReadAllText(@"c:\temp\drawing2.txt");
            Assert.IsTrue(content1 == content2);
        }
    }
}
