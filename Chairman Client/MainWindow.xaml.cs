﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Chairman_Client.Model;
using System.Threading.Tasks;

namespace Chairman_Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Model.Context context;
        private System.IO.FileSystemWatcher _watcher;
        private Timer _timer;
        private bool _playing;

        public MainWindow()
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException +=
                    new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
                InitializeComponent();
                // TestCode_
                context = new Context();
                var stream = OpenStream(ChairmanSettings.Default.Path + "\\Reductions.txt");
                context.LoadReductions(stream);
                stream.Close();
                stream = OpenStream(ChairmanSettings.Default.Path + "\\dance.txt");
                context.LoadCouples(stream);
                stream.Close();

                this.ReductionList.ItemsSource = context.Reductions;

                _watcher = new FileSystemWatcher(ChairmanSettings.Default.VideoPath);
                _watcher.Created += new FileSystemEventHandler(VideoCreated);
                _watcher.EnableRaisingEvents = true;

                this.DataContext = context;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                if (ex.InnerException != null)
                    MessageBox.Show(ex.InnerException.Message);
            }
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if(e.ExceptionObject is Exception)
            {
                var ex = (Exception) e.ExceptionObject;
                MessageBox.Show(ex.Message);
                MessageBox.Show(ex.StackTrace);
            }
        }

        void VideoCreated(object sender, FileSystemEventArgs e)
        {
            // Sicherstellen, dass die Datei komplett geschrieben ist
            var stream = OpenStream(e.FullPath);

            if (stream == null)
            {
                MessageBox.Show("Could not open Video file");
                return;
            }

            stream.Close();
            MediaPlayer.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(MediaPlayer_MediaFailed);
            _playing = true;
            this.Dispatcher.Invoke( (Action)(() => { MediaPlayer.Source = new Uri(e.FullPath);
                                                     MediaPlayer.Play();
                                                       // MediaOpened(null, null);
            } )) ;
        }

        void MediaPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            MessageBox.Show(e.ErrorException.Message);
            _playing = false;
        }

        private System.IO.StreamReader OpenStream(string file)
        {
            int counter = 0;
            System.IO.StreamReader reader = null;

            while (reader == null && counter < 240) // Langer Timeout, da das Kopieren des Video lange dauern kann
            {
                try
                {
                    reader = new StreamReader(file);
                }
                catch (Exception)
                {
                    counter++;
                    System.Threading.Thread.Sleep(500);
                }
            }

            return reader;
        }

        private void MediaOpened(object sender, System.Windows.RoutedEventArgs e)
        {
            TimeSlider.Maximum = MediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            // MediaPlayer.Play();
            _timer = new Timer();
            _timer.Elapsed += new ElapsedEventHandler(VideoTimer);
            _timer.Interval = 1000;
            _timer.Enabled = true;
            _playing = true;
            PlayButton.Content = "Pause";
        }

        void VideoTimer(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke((Action)(() => { TimeSlider.Value = MediaPlayer.Position.TotalMilliseconds; }));
            
        }

        private void TimeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int SliderValue = (int)TimeSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds.
            // Create a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);

            var div = MediaPlayer.Position.Subtract(ts).TotalSeconds;
            Console.WriteLine(div);
            if(div > 2 || div < -2)
                MediaPlayer.Position = ts;
        }

        private void Window_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Space && MediaPlayer.Position.TotalSeconds > 10)
            {
                var ts = new TimeSpan(0,0,0,0, ((int)MediaPlayer.Position.TotalMilliseconds - 10000));
                MediaPlayer.Position = ts;
            }
        }

        private void ReductionList_SourceUpdated(object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            Console.WriteLine(e.Property);
        }

        private void ButtonConfirm_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // Save Data Asynchron
            Task task = new Task(() =>
                                     {
                                         SaveData();
                                     }
                );

            task.Start();

            if (!context.NextCouple())
                ButtonConfirm.IsEnabled = false;
        }

        private void SaveData()
        {
            try
            {
                // Wir speichern das ganze lokal und im Netzwerk auf dem Server
                var writer = new StreamWriter(ChairmanSettings.Default.LocalOutPath + "\\ChairmanResult.txt");
                context.WriteResult(writer);
                writer.Close();
                writer = new StreamWriter(ChairmanSettings.Default.OutPath + "\\ChairmanResult.txt");
                context.WriteResult(writer);
                writer.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        private void Window_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
        	if (e.Key == Key.Space && MediaPlayer.Position.TotalSeconds > 10)
            {
                var ts = new TimeSpan(0,0,0,0, ((int)MediaPlayer.Position.TotalMilliseconds - 10000));
                MediaPlayer.Position = ts;
				e.Handled = true;
            }
        }

        private void PlayButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_playing)
            {
                MediaPlayer.Pause();
                _playing = false;
                PlayButton.Content = "Play";
            }
            else
            {
                MediaPlayer.Play();
                _playing = true;
                PlayButton.Content = "Pause";
            }
        }

        private void Slider_ValueChanged(object sender, System.Windows.RoutedPropertyChangedEventArgs<double> e)
        {
            MediaPlayer.SpeedRatio = e.NewValue;
        }

    }
}
