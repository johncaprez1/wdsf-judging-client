﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Threading;
using System.Threading;

using Beamer_Client.Properties;

using CommonLibrary;

using Application = System.Windows.Application;
using Cursors = System.Windows.Input.Cursors;
using Image = System.Windows.Controls.Image;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using MessageBox = System.Windows.MessageBox;

namespace Beamer_Client
{
    public class ShowdanceResult
    {
        public string Judge { get; set; }
        public double MarkA { get; set; }
        public double MarkB { get; set; }

        public ShowdanceResult(string judge, string markA, string markB)
        {
            var sepchar = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            Judge = judge;
            MarkA = markA.ParseString();
            MarkB = markB.ParseString();
        }
    }

    public class ShowDanceData
    {
        public string Number { get; set; }
        public string Couple { get; set; }
        public string Theme { get; set; }
        public string Choreograph { get; set; }
        public string Country { get; set; }
    }


    public class Qualified
    {
        public int Number { get; set; }
        public string Couple { get; set; }
        public string Country { get; set; }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string currentDanceName;

        private string currentNumber;

        private bool useFirstBrowserHost = true;

        public MainWindow()
        {
            InitializeComponent();

            Application.Current.DispatcherUnhandledException += new DispatcherUnhandledExceptionEventHandler(Current_DispatcherUnhandledException);

            this.Width = BeamerClientSettings.Default.Width;
            this.Height = BeamerClientSettings.Default.Height;
            this.Left = BeamerClientSettings.Default.Left;
            this.Top = BeamerClientSettings.Default.Top;

            if (BeamerClientSettings.Default.FullScreen)
            {
                this.Topmost = true;
                this.WindowState = WindowState.Maximized;
                Mouse.OverrideCursor = Cursors.Help;
            }
            else
            {
                this.WindowStyle = WindowStyle.None;
                this.BorderThickness = new Thickness(0);
            }

            this.BrowserHost1.LoadCompleted += BrowserHost_Navigated;
            this.BrowserHost2.LoadCompleted += BrowserHost_Navigated;
            
            ShowBlank();

            this.StateChanged += (sender, args) =>
                {
                    // if someone minimized us, we try to reactivate us
                    // otherwise it looks stupid
                    if (this.WindowState == WindowState.Minimized)
                    {
                        this.WindowState = WindowState.Maximized;
                        this.Topmost = true;
                        this.Activate();
                    }
                };
        }

        void BrowserHost_Navigated(object sender, NavigationEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.None;

            if (sender == BrowserHost1)
            {
                BrowserHost1.Visibility = Visibility.Visible;
                BrowserHost2.Visibility = Visibility.Hidden;
            }
            if (sender == BrowserHost2)
            {
                BrowserHost2.Visibility = Visibility.Visible;
                BrowserHost1.Visibility = Visibility.Hidden;
            }

            var task = new Task(
                () =>
                {
                    Thread.Sleep(1000);

                    this.Dispatcher.Invoke((Action)(() => this.TakeScreenShot(this.currentDanceName, this.currentNumber)));
                });

            task.Start();
        }

        void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show(e.Exception.Message);
            MessageBox.Show(e.Exception.StackTrace);
        }


        void FileSystemWatcherChanged(object sender, FileSystemEventArgs e)
        {
            // Abhängig von der Datei lesen wir nun ein, was angezeigt werden soll
            switch(e.Name)
            {
                case "DanceResult.txt":
                    ShowDanceResult(e.FullPath);
                    break;
                case "DanceTotal.txt":
                    ShowDanceTotal(e.FullPath);
                    break;
                case "TotalResult.txt":
                    ShowTotalResult(e.FullPath);
                    break;
                case "ClearResult.txt":
                    ShowBlank();
                    break;
            }
        }

        void ShowHtml(string html)
        {
            // Wir speichern die Daten nun einmal extern, damit sie notfalls auch 
            // über einen anderen Client angezeigt werden kann.
            try
            {
                var outS = new StreamWriter(BeamerClientSettings.Default.PathOut + "\\htmlfile.html");
                outS.WriteLine(html);
                outS.Close();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }

            this.Dispatcher.BeginInvoke(DispatcherPriority.Input, new ThreadStart(() =>
            {
                if (useFirstBrowserHost)
                {
                    BrowserHost1.Navigate("file://" + BeamerClientSettings.Default.PathOut + "\\htmlfile.html");
                }
                else
                {
                    BrowserHost2.Navigate("file://" + BeamerClientSettings.Default.PathOut + "\\htmlfile.html");
                }

                useFirstBrowserHost = !useFirstBrowserHost;
            }));
        }

        string LoadHtmlFile(string file)
        {
            string html = File.ReadAllText(file);
            string css = File.ReadAllText("HTML\\style.css");
            html = html.Replace("@@@CSS@@@", css);
            return html;
        }

        string[] LoadDataFile(string file)
        {
            string[] res = null;
            int count = 0;

            while (res == null && count < 20)
            {
                try
                {
                    res = File.ReadAllLines(file, Encoding.UTF8);
                    if (res.Length == 0)
                    {
                        res = null;
                        throw new Exception("No data");
                    }
                    return res;
                }
                catch (Exception)
                {
                    count++;
                    Thread.Sleep(300);
                }
            }
            return new string[1];
        }

        void ShowBlank()
        {
            string html = LoadHtmlFile("HTML\\Blank.htm");
            ShowHtml(html);
        }

        void ShowDanceResult(string file)
        {
            string html = LoadHtmlFile("HTML\\DanceResult.htm");
            // Nun bauen wir das Ergebnis auf und zeigen es an
            string[] lines = LoadDataFile(file);
            if (lines.Length < 2)
                return; // Keien Daten geladen

            string[] data;

            data = lines[2].Split(';');

            html = html.Replace("@@@A@@@", data[0]);
            html = html.Replace("@@@B@@@", data[1]);
            html = html.Replace("@@@C@@@", data[2]);
            html = html.Replace("@@@D@@@", data[3]);
            
            data = lines[3].Split(';');
            html = html.Replace("@@@Average@@@", data[0]);
            html = html.Replace("@@@Place@@@", data[1]);
            
            data = lines[1].Split(';');
            var coupleData = data[1].Split('|');
            var couple = coupleData[0];
            var theme = coupleData.Length > 1 ? coupleData[1] : "";
            var choreograph = coupleData.Length > 2 ? coupleData[2] : "";

            html = html.Replace("@@@Number@@@", data[0]);
            html = html.Replace("@@@Couple@@@", couple.Replace("_", " ").Replace("/", " / "));
            html = html.Replace("@@@Theme@@@", theme);
            html = html.Replace("@@@Choreograph@@@", choreograph);
            html = html.Replace("@@@Country@@@", data[2]);
            html = html.Replace("@@@Number@@@", data[0]);

            var dances = lines[0].Split(';');
            html = html.Replace("@@@Dance@@@", dances[1]);
            ShowHtml(html);

            this.currentDanceName = dances[1];
            this.currentNumber = data[0];
        }

        void ShowDanceTotal(string file)
        {
            string html = LoadHtmlFile("HTML\\DanceTotal.htm");
            // Nun bauen wir das Ergebnis auf und zeigen es an
            string table = "";
            string[] lines = LoadDataFile(file);
            string[] data;

            for (int i = 1; i < lines.Length; i++)
            {
                data = lines[i].Split(';');
                table += "<tr class=\"ResultRow\">";
                table += "<td class=\"ResultPlace\">" + data[0] + ". </td>" + 
                         "<td class=\"ResultCouple\">" + data[3].Replace("_", " ").Replace("/", " / ") + "</td>" + 
                         "<td class=\"ResultCountry\">" + data[4] + "</td>" +
                         "<td class=\"ResultPoints\">" + data[1] + "</td>";

                table += "</tr>";
            }
            var dances = lines[0].Split(';');
            html = html.Replace("@@@Result@@@", table);
            html = html.Replace("@@@Dance@@@", dances[1]);
            ShowHtml(html);
        }

        private void ShowFlags(string file)
        {
            string html = LoadHtmlFile("HTML\\Flags.htm");

            string[] lines = LoadDataFile(file);
            string[] data;

            for (int i = 0; i < 3; i++)
            {
                data = lines[i].Split(';');

                html = html.Replace("@@@Country" + (i + 1) + "@@@", data[4]);
            }

            ShowHtml(html);
        }

        void ShowTotalResult(string file)
        {
            string html = LoadHtmlFile("HTML\\TotalResult.htm");
            // Nun bauen wir das Ergebnis auf und zeigen es an
            string table = "";
            string[] lines = LoadDataFile(file);
            string[] data;

            for (int i = 0; i < lines.Length; i++)
            {
                data = lines[i].Split(';');
                table += "<tr class=\"ResultRow\">";

                var coupleData = data[3].Split('|');
                var couple = coupleData[0];

                table += "<td class=\"ResultPlace\">" + data[0] + ". </td>" +
                         "<td class=\"ResultCouple\">" + couple.Replace("_", " ").Replace("/", " / ") + "</td>" +
                         "<td class=\"ResultCountry\">" + data[4] + "</td>" +
                         "<td class=\"ResultPoints\">" + data[1] + "</td>";
                table += "</tr>\r\n";
            }

            html = html.Replace("@@@Result@@@", table);
            ShowHtml(html);
        }

        private ShowDanceData LoadShowDanceData(string file)
        {
            if (!File.Exists(file)) return null;

            var reader = new StreamReader(file);
            reader.ReadLine();

            var data = reader.ReadLine().Split(';');

            var coupleData = data[1].Split('|');

            var result = new ShowDanceData()
            {
                Number = data[0],
                Country = data[2],
                Couple = coupleData[0],
                Theme = coupleData.Length > 1 ? coupleData[1] : "",
                Choreograph = coupleData.Length > 2 ? coupleData[2] : ""
            };

            reader.Close();
            
            return result;
        }

        void ShowQualified(string file)
        {
            string html = LoadHtmlFile("HTML\\Qualified.htm");
            // Nun bauen wir das Ergebnis auf und zeigen es an
            string table = "";
            string[] lines = LoadDataFile(file);
            string[] data;

            var qualified = new List<Qualified>();

            for (int i = 0; i < lines.Length; i++)
            {
                data = lines[i].Split(';');
                var coupleData = data[3].Split('|');
                var couple = coupleData[0];

                qualified.Add(new Qualified()
                                  {
                                      Couple = couple,
                                      Number = int.Parse(data[2]),
                                      Country = data[4]
                                  });

            }

            foreach (var qual in qualified.OrderBy(o => o.Number))
            {
                
            table += "<tr class=\"QualifiedRow\">";
                table += "<td class=\"QualifiedNumber\">" + qual.Number + "</td>";
                table += "<td class=\"QualifiedName\">" + qual.Couple.Replace("_", " ").Replace("/", " / ") + "</td>";
                table += "<td class=\"QualifiedCountry\">" + qual.Country + "</td>";
                table += "</tr>\r\n";
            }

            html = html.Replace("@@@Result@@@", table);
            ShowHtml(html);
        }

        void ShowShowDanceQualified(string file)
        {
            var data = LoadShowDanceData(file);
            if (data == null) return;

            string html = LoadHtmlFile("HTML\\ShowdanceQualfied.htm");

            html = html.Replace("@@@Theme@@@", data.Theme);
            html = html.Replace("@@@Couple@@@", string.Format("{0} ({1})", data.Couple, data.Country));
            html = html.Replace("@@@Number@@@", data.Number);

            if (!string.IsNullOrEmpty(data.Choreograph))
            {
                html = html.Replace("@@@Choreograph@@@", string.Format("Choreograph: {0}", data.Choreograph));
            }
            else
            {
                html = html.Replace("@@@Choreograph@@@", "");
            }

            ShowHtml(html);
        }

        void ShowShowDanceDetails(string file)
        {
            var data = LoadShowDanceData(file);
            if (data == null) return;

            string html = LoadHtmlFile("HTML\\ShowdanceDetails.htm");

            html = html.Replace("@@@Number@@@", data.Number);
            html = html.Replace("@@@Couple@@@", data.Couple);

            

            ShowHtml(html);
        }
        /// <summary>
        /// Steuerung per Tastatur, um beispielsweise zu Beenden oder Screen zu löschen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.E: Close();
                    break;
                case Key.Back:
                    ShowBlank();
                    break;
                case Key.C:
                    ShowDanceResult(BeamerClientSettings.Default.Path + "\\DanceResult.txt");
                    break;
                case Key.D:
                    ShowDanceTotal(BeamerClientSettings.Default.Path + "\\DanceTotal.txt");
                    break;
                case Key.T:
                    ShowTotalResult(BeamerClientSettings.Default.Path + "\\TotalResult.txt");
                    break;
                case Key.Q:
                    ShowQualified(BeamerClientSettings.Default.Path + "\\TotalResult.txt");
                    break;
                case Key.F:
                    ShowFlags(BeamerClientSettings.Default.Path + "\\TotalResult.txt");
                    break;
                case Key.S:
                    ShowShowDanceQualified(BeamerClientSettings.Default.Path + "\\DanceResult.txt");
                    break;
                case Key.K:
                    ShowShowDanceDetails(BeamerClientSettings.Default.Path + "\\ResultShowDance.txt");
                    break;
                case Key.D6:
                    ShowQualifiedTop(6, BeamerClientSettings.Default.Path + "\\TotalResult.txt");
                    break;
                case Key.D7:
                    ShowQualifiedTop(7, BeamerClientSettings.Default.Path + "\\TotalResult.txt");
                    break;
            }
        }



        private void ShowQualifiedTop(int maxCouples, string file)
        {
            string html = LoadHtmlFile("HTML\\Qualified.htm");
            // Nun bauen wir das Ergebnis auf und zeigen es an
            string table = "";
            string[] lines = LoadDataFile(file);
            string[] data;

            for (int i = 0; i < maxCouples; i++)
            {
                data = lines[i].Split(';');
                var coupleData = data[3].Split('|');
                var couple = coupleData[0];

                table += "<tr class=\"QualifiedRow\">";
                table += "<td class=\"QualifiedNumber\">" + data[2] + "</td>";
                table += "<td class=\"QualifiedName\">" + couple.Replace("_", " ").Replace("/", " / ") + "</td>";
                table += "<td class=\"QualifiedCountry\">" + data[4] + "</td>";
                table += "</tr>\r\n";
            }

            html = html.Replace("@@@Result@@@", table);
            ShowHtml(html);
        }

        private void TakeScreenShot(string coupleNumber, string danceName)
        {
            if (coupleNumber == null || danceName == null)
            {
                return;
            }

            var fileName = string.Format(
                "{0}\\{1}_{2}.jpg",
                BeamerClientSettings.Default.PathOut,
                danceName,
                coupleNumber);

            var screenIndex = BeamerClientSettings.Default.ScreenIndex;

            if (screenIndex < 0 || screenIndex >= Screen.AllScreens.Count())
            {
                return;
            }

            var bitmap = new Bitmap(Screen.AllScreens[screenIndex].Bounds.Width, Screen.AllScreens[screenIndex].Bounds.Height);

            //var bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            var graphics = Graphics.FromImage(bitmap);

            graphics.CopyFromScreen(Screen.AllScreens[screenIndex].Bounds.Left, Screen.AllScreens[screenIndex].Bounds.Top, 0, 0, bitmap.Size);

            bitmap.Save(fileName, ImageFormat.Jpeg);
        }
    }
}
